package com.example.qmax.adda.BLE;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.widget.Toast;

import com.example.qmax.adda.BLE.BLE.DeviceControlActivity;
import com.example.qmax.adda.R;

/**
 * Created by Buvaneswari on 6/1/16.
 */

public class ADDAApplication extends MultiDexApplication {

    public static final String PREF_DEVICE_DETAILS = "pref_device_details";
    public static final String PREF_DEVICE_NAME = "pref_device_name";
    public static final String PREF_DEVICE_ADDRESS = "pref_device_address";
    public static final String PREF_DEVICE_CONNECTION_STATUS = "pref_device_connection_status";
    public static final String PREF_DEVICE_CONNECTION_STATUS_KEY = "pref_device_connection_status_key";
    public static final String STATUS_CONNECTED = "CONNECTED";
    public static final String STATUS_DISCONNECTED = "DISCONNECTED";
    private static final String TAG = ADDAApplication.class.getSimpleName();

    public static int FLAG_REQUEST_DEVICE_STATUS = 0;
    public static String mDeviceAddress = "88:4A:EA:3B:76:55";


    public static byte[] BYTE_SEND_OPEN_DOOR = {0x41, 0x54, 0x01, 0x01, 0x01,
            0x02, 0x03, 0x04, 0x06, 0x07,
            0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
            0x20, 0x0D, 0x0A};

    @Override
    public void onCreate() {
        super.onCreate();
        checkDatabase();
    }

    private void checkDatabase() {
        /**
         * Is first time install app to store setting default specification in table
         */
        SharedPreferences prefs = getSharedPreferences(getApplicationContext().getResources().getString(R.string.firsttimeprefs), MODE_PRIVATE);
        boolean firstRun = prefs.getBoolean(getApplicationContext().getResources().getString(R.string.isfirsttime), true);
        if (firstRun) {
            Log.d(TAG, "Application Installed at first time");

            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(getApplicationContext().getResources().getString(R.string.isfirsttime), false);
            editor.apply();
            addShortcut();

            SharedPreferences sharedpreferences = getSharedPreferences(PREF_DEVICE_DETAILS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor_seq_no = sharedpreferences.edit();
            editor_seq_no.clear();
            editor_seq_no.putString(PREF_DEVICE_NAME, "");
            editor_seq_no.putString(PREF_DEVICE_ADDRESS, "");
            editor_seq_no.putString(PREF_DEVICE_CONNECTION_STATUS, "");
            editor_seq_no.commit();

            SharedPreferences device_status = getSharedPreferences(PREF_DEVICE_CONNECTION_STATUS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor_ = device_status.edit();
            editor_.clear();
            editor_.putString(PREF_DEVICE_CONNECTION_STATUS_KEY, "");
            editor_.commit();

        }
    }

    private void addShortcut() {
        //on Home screen
        Intent shortcutIntent = new Intent(getApplicationContext(), DeviceControlActivity.class);
        shortcutIntent.setAction(Intent.ACTION_MAIN);
        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getResources().getString(R.string.app_name));
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.mipmap.ic_launcher));
        addIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent);
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent);
        Log.d(TAG, "Application ShortCut Created");
        Toast.makeText(getApplicationContext(), getString(R.string.app_name) + " Shortcut created", Toast.LENGTH_SHORT).show();
    }
}
